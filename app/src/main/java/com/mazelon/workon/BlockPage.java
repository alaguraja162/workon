package com.mazelon.workon;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

import static com.mazelon.workon.Constants.BASE_URL;

/**
 * Created by alaguraja on 06/02/18.
 */

public class BlockPage extends AppCompatActivity {

    Button unblock_req_btn;
    JSONParser jsonParser = new JSONParser();
    private String url = BASE_URL+"Home/app/app_unblock_request.php";
    int flag=0;
    SharedPreferences unamepref;
    SharedPreferences.Editor unameedt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.blockpage);

        unblock_req_btn = (Button) findViewById(R.id.unblock_req_btn);
        unblock_req_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new unblockRequest().execute();
            }
        });

    }

    class unblockRequest extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... arg0) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            SharedPreferences username_sp = getSharedPreferences("SharedPrefName", MODE_PRIVATE);
            String username =username_sp.getString("username","");
            params.add(new BasicNameValuePair("username", username));
            params.add(new BasicNameValuePair("logout", "Logged Out"));
            JSONObject json = jsonParser.makeHttpRequest(url, "POST", params);

            Log.d("Create Response", json.toString());

            try {
                int success = json.getInt("success");
                String status = json.getString("status");
                if (success == 1) {
                    flag = 0;
                    if(status.equals("Inactive")){
                        new logoutAccess().execute();
                    }
//                    Toast.makeText(BlockPage.this,"Requested Successfully", Toast.LENGTH_LONG).show();
                } else {
                    // failed to login
                    flag = 1;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(flag==1)
                Toast.makeText(BlockPage.this,"Error..", Toast.LENGTH_LONG).show();

        }

    }

    class logoutAccess extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... arg0) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            SharedPreferences username_sp = getSharedPreferences("SharedPrefName", MODE_PRIVATE);
            String username =username_sp.getString("username","");
            params.add(new BasicNameValuePair("username", username));
            params.add(new BasicNameValuePair("logout", "Logged Out"));
            JSONObject json = jsonParser.makeHttpRequest(url, "POST", params);

            Log.d("Create Response", json.toString());

            try {
                int success = json.getInt("success");
                if (success == 1) {
                    flag = 0;
                    unamepref= getSharedPreferences("SharedPrefName", MODE_PRIVATE);
                    unameedt = unamepref.edit();
                    unameedt.putString("username", "");
                    unameedt.apply();
                    startActivity(new Intent(BlockPage.this, LoginActivity.class));
                    finish();
                } else {
                    // failed to login
                    flag = 1;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(flag==1)
                Toast.makeText(BlockPage.this,"Error..", Toast.LENGTH_LONG).show();

        }

    }
}
