package com.mazelon.workon;

import android.app.AppOpsManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import studio.carbonylgroup.textfieldboxes.ExtendedEditText;

import static com.mazelon.workon.Constants.BASE_URL;


public class LoginActivity extends AppCompatActivity {

    String username,password;
    ExtendedEditText username_et,password_et;
    Button login_btn;
    ProgressDialog progressDialog;
    private String url = BASE_URL+"app/app_login.php";
    SharedPreferences unamepref;
    SharedPreferences.Editor unameedt;
    JSONParser jsonParser = new JSONParser();
    int flag=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

        if (!isAccessGranted()) {
            Toast.makeText(getApplicationContext(),"Grant Usage Access To Continue",Toast.LENGTH_LONG).show();
            Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
            startActivity(intent);
        }

        SharedPreferences IsLogggedIn = getSharedPreferences("SharedPrefName", MODE_PRIVATE);
        String logged =IsLogggedIn.getString("username","");
        if(logged.length()!=0)
        {
            Intent intent = new Intent(LoginActivity.this,Home.class);
            startActivity(intent);
        }

        username_et = findViewById(R.id.username_et);
        password_et = findViewById(R.id.password_et);
        login_btn = findViewById(R.id.login_btn);

        username_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                username = editable.toString();
            }
        });

        password_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                password = editable.toString();
            }
        });

        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isAccessGranted()) {
                    Toast.makeText(getApplicationContext(),"Grant Usage Access To Continue",Toast.LENGTH_LONG).show();
                } else {
                    //check connectivity
                    if (!isOnline(LoginActivity.this)) {
                        Toast.makeText(LoginActivity.this, "No network connection", Toast.LENGTH_LONG).show();
                    } else {
                        new loginAccess().execute();
                    }
                }
            }
        });

    }

    private boolean isAccessGranted() {
        try {
            PackageManager packageManager = getPackageManager();
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(getPackageName(), 0);
            AppOpsManager appOpsManager = (AppOpsManager) getSystemService(Context.APP_OPS_SERVICE);
            int mode = 0;
            if (appOpsManager != null) {
                mode = appOpsManager.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS,
                            applicationInfo.uid, applicationInfo.packageName);
            }

            return (mode == AppOpsManager.MODE_ALLOWED);

        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    //code to check online details
    private boolean isOnline(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = null;
        if (cm != null) {
            netInfo = cm.getActiveNetworkInfo();
        }
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    class loginAccess extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... arg0) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("username", username));
            params.add(new BasicNameValuePair("password", password));
            JSONObject json = jsonParser.makeHttpRequest(url, "POST", params);

            Log.e("params",params.toString());
            Log.d("Create Response", json.toString());

            try {
                int success = json.getInt("success");
                if (success == 1) {
                    flag = 0;
                    Intent i = new Intent(getApplicationContext(), Home.class);
                    unamepref= getSharedPreferences("SharedPrefName", MODE_PRIVATE);
                    unameedt = unamepref.edit();
                    unameedt.putString("username", username);
                    unameedt.apply();
                    startActivity(i);
                    finish();
                } else {
                    // failed to login
                    flag = 1;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(flag==1)
                Toast.makeText(LoginActivity.this,"Please Enter Correct informations", Toast.LENGTH_LONG).show();

        }

    }


}
