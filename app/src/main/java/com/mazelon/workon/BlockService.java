package com.mazelon.workon;

import android.app.ActivityManager;
import android.app.Service;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;

import static android.content.ContentValues.TAG;
import static com.mazelon.workon.Constants.BASE_URL;

/**
 * Created by alaguraja on 05/02/18.
 */

public class BlockService extends Service
{

    int delay = 0;
    Timer timer =new Timer();
    myTimer mTask= new myTimer();
    private Context ctx;
    private static String urls = BASE_URL+"app/app_block.php";
    ArrayList<AppListing> appstobeBlocked = new ArrayList<>();

    public IBinder onBind(Intent arg0)
    {
        return null;
    }

    public void onCreate()
    {
        super.onCreate();
        ctx = this;
        startService();
    }

    private void startService()
    {
        timer.scheduleAtFixedRate(mTask, delay, 100);
    }

    private class myTimer extends TimerTask
    {
        public void run()
        {
            toastHandler.sendEmptyMessage(0);
        }
    }

    public void onDestroy()
    {
        super.onDestroy();
        //Toast.makeText(this, "Service Stopped ...", Toast.LENGTH_SHORT).show();
        timer.cancel();
    }

    public class LoadApplicationTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        protected Boolean doInBackground(Void... params) {

            HttpHandler sh = new HttpHandler();

            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(urls);

            //Log.e(TAG, "Response from url: " + jsonStr);

            if (jsonStr != null) {
                try {
                    JSONArray jsonarray = new JSONArray(jsonStr);
                    for (int i = 0; i < jsonarray.length(); i++) {
                        // Getting JSON Array node
                        JSONObject jsonObject = jsonarray.getJSONObject(i);
                        //Log.e(TAG, "JSON Object: " + jsonObject);
                        AppListing applist = new AppListing();
                        if (!jsonObject.isNull("package_name")) {
                            applist.name = jsonObject.getString("package_name");
                        }

                        appstobeBlocked.add(i, applist);
                    }
                } catch (final JSONException e) {
                    //Log.e(TAG, "Json parsing error: " + e.getMessage());

                }
            } else {
                //Log.e(TAG, "Couldn't get json from server.");
            }

            return true;
        }

        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
        }

    }

    public static class AppListing {
        String name;
        AppListing() {
        }
    }


    private final Handler toastHandler = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            new LoadApplicationTask().execute();
            String currentApp = "NULL";
            if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                UsageStatsManager usm = (UsageStatsManager) ctx.getSystemService(Context.USAGE_STATS_SERVICE);
                long time = System.currentTimeMillis();
                List<UsageStats> appList = usm.queryUsageStats(UsageStatsManager.INTERVAL_DAILY,  time - 1000*1000, time);
                if (appList != null && appList.size() > 0) {
                    SortedMap<Long, UsageStats> mySortedMap = new TreeMap<Long, UsageStats>();
                    for (UsageStats usageStats : appList) {
                        mySortedMap.put(usageStats.getLastTimeUsed(), usageStats);
                    }
                    if (mySortedMap != null && !mySortedMap.isEmpty()) {
                        currentApp = mySortedMap.get(mySortedMap.lastKey()).getPackageName();
                    }
                }
            } else {
                ActivityManager am = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);
                List<ActivityManager.RunningAppProcessInfo> tasks = am.getRunningAppProcesses();
                currentApp = tasks.get(0).processName;
            }

            //Log.e(TAG, "Current App in foreground is: " + currentApp);

            for(int i=0;i<appstobeBlocked.size();i++){
                if(currentApp.equals(appstobeBlocked.get(i).name)){
                    Intent intent = new Intent(BlockService.this,BlockPage.class);
                    startActivity(intent);
                }
            }

        }
    };

}
